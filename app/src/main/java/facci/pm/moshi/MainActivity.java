package facci.pm.moshi;

import static facci.pm.moshi.Suit.CLUBS;
import static facci.pm.moshi.Suit.HEARTS;
import static facci.pm.moshi.Suit.SPADES;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //FROM JSON TO OBJECTS
        //CONTENIDO JSON ASIGNADO A UNA VARIABLE DE TIPO STRING
        String json = "{\"hidden_card\":{\"rank\":\"6\",\"suit\":\"SPADES\"},\"visible_cards\":[{\"rank\":\"4\",\"suit\":\"CLUBS\"},{\"rank\":\"A\",\"suit\":\"HEARTS\"}]}";

        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<BlackjackHand> jsonAdapter = moshi.adapter(BlackjackHand.class);

        BlackjackHand blackjackHand = null;
        try {
            blackjackHand = jsonAdapter.fromJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
        blackjackHand.visible_cards.forEach((x)->{
                Log.e("item", x.suit.toString());
        });

        //FROM OBJECTS TO JSON
//        BlackjackHand blackjackHand = new BlackjackHand(
//                new Card('6', SPADES),
//                Arrays.asList(
//                        new Card('4', CLUBS),
//                        new Card('A', HEARTS)));
//
//        Moshi moshi = new Moshi.Builder().build();
//        JsonAdapter<BlackjackHand> jsonAdapter = moshi.adapter(BlackjackHand.class);
//
//        String json = jsonAdapter.toJson(blackjackHand);
////        System.out.println(json);
//        Log.e("From OBJECTS to JSON", json);

    }
}